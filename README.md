# BioMedIT Ansible Collection

## Installation

```shell
ansible-galaxy collection install git+https://gitlab.com/biomedit/tools/devops.git
```

Alternatively you can create `requirements.yml` file, e.g.

```yaml
---
collections:
  - name: https://gitlab.com/biomedit/tools/devops.git
    type: git
    version: main
```

and install it with

```shell
ansible-galaxy install -r requirements.yml
```

By default new collections are installed into `~/.ansible/collections`.

For more information about installing collections see the
[official documentation](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html#installing-collections).

## Usage

To use a `biomedit.devops` role in a playbook write

```yaml
---
- hosts: all
  roles:
    - role: biomedit.devops.docker
      become: yes
```
